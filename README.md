WEATHER API
------------

### Install 

1. Get project
2. Create database, name: weather
3. Run 'yii migrate' or 'php yii migrate'
4. Set country on db: POST request to {url}/country/create with json params code and name(optional)
5. Set city on db: POST request to {url}/city/create with json params name and country_id(get id from previous step)
6. Set weather on db: POST request to {url}/weather/create with json params "city_id"(get id from previous step), "for_date", "temperature"

------------
### Usage
1. GET request to {url}/info/view with json params "city", "country" - to get city weather info
2. GET request to {url}//info/user-requests with json params "from" and "to" - to get user request statistics by cities

