<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%weather}}`.
 */
class m191128_153245_create_weather_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%weather}}', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->notNull(),
            'for_date' => $this->date(),
            'temperature' => $this->integer()->notNull(),
        ]);

        $this->createIndex(
            'idx-weather-city_id',
            'weather',
            'city_id'
        );

        $this->addForeignKey(
            'fk-weather-city_id',
            'weather',
            'city_id',
            'city',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-weather-city_id',
            'weather'
        );

        $this->dropIndex(
            'idx-weather-city_id',
            'weather'
        );

        $this->dropTable('{{%weather}}');
    }
}
