<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_request}}`.
 */
class m191127_184233_create_user_request_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_request}}', [
            //'id' => $this->primaryKey(),
            'city_id' => $this->integer(),
            'created_at' => $this->date(),
        ]);

        $this->createIndex(
            'idx-user_request-city_id',
            'user_request',
            'city_id'
        );

        $this->addForeignKey(
            'fk-user_request-city_id',
            'user_request',
            'city_id',
            'city',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-user_request-city_id',
            'user_request'
        );

        $this->dropIndex(
            'idx-user_request-city_id',
            'user_request'
        );

        $this->dropTable('{{%user_request}}');
    }
}
