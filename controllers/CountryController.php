<?php

namespace app\controllers;

use yii\rest\ActiveController;

class CountryController extends ActiveController
{
    public $modelClass = 'app\models\Country';

    public function actionIndex()
    {
        return 'hereo'; //$this->render('index');
    }

    public function actionCreate()
    {
        return 'created'; //$this->render('create');
    }
}
