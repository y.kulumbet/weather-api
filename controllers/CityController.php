<?php

namespace app\controllers;

use yii\rest\ActiveController;

class CityController extends ActiveController
{
    public $modelClass = 'app\models\City';

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCreate()
    {
        return 'created'; //$this->render('create');
    }
}
