<?php

namespace app\controllers;

use yii\rest\ActiveController;

class WeatherController extends ActiveController
{
    public $modelClass = 'app\models\Weather';

    public function actionCreate()
    {
        return $this->render('create');
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
}
