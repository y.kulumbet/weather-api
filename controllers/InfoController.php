<?php

namespace app\controllers;

use Yii;
use yii\rest\Controller;
use app\models\Weather;
use app\models\City;
use app\models\Country;
use app\models\UserRequest;
use app\models\Traits\Helper;

class InfoController extends Controller
{
    use Helper;

    public function actionIndex()
    {
        return 'here';
    }

    public function actionView()
    {
        $request = Yii::$app->request;

        $model = new Country;
        $model->code =  $request->getBodyParam('country');
        
        if (!$model->validate()) {           
            if (!strpos($model->getFirstError('code'), 'уже существует') !== false) {
                return ['error' => $model->getFirstError('code')];    
            }            
        }

        $model = new City;
        $model->name =  $request->getBodyParam('city');        
        if (!$model->validate('name')) {                       
            return ['error' => $model->getFirstError('name')];                            
        }

        $country = Country::find()->where(['code' => $request->getBodyParam('country')])->one();
        if (!$country) {                       
            return ['error' => 'К сожаления такая страна отсуствует.'];                            
        }

        $city = City::find()->where(['name' => $request->getBodyParam('city'), 'country_id' => $country->id])->one();
        if (!$city) {                       
            return ['error' => 'К сожаления такой город отсуствует.'];                            
        }

        $weather = Weather::find()
            ->where(['city_id' => $city->id, 'for_date' => date("Y-m-d")])
            ->one();
            
        if ($weather) {
            $userRequest = new UserRequest();
            $userRequest->city_id = $city->id;
            $userRequest->created_at = date("Y-m-d");
            $userRequest->save();
        } else {
            return ['error' => 'К сожаления данные на сегодня на этот город отсуствует.'];
        }

        return $weather;
    }

    # receives 2 params: from, to
    public function actionUserRequests()
    {
        $request = Yii::$app->request;

        list($from, $to) = $this->formatDate($request->getBodyParam('from'), $request->getBodyParam('to')); 
        
        $userRequests = UserRequest::find()->where(['between', 'created_at', $from, $to])->all();

        $response = $this->calculateCityRequests($userRequests);
        
        return $response;
    }
}
