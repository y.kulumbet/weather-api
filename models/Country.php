<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "country".
 *
 * @property int $id
 * @property string $code
 * @property string|null $name
 *
 * @property City[] $cities
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code'], 'required', 'message' => '{attribute} не может быть пустым.'],
            [['code'], 'string', 'min' => 2,  'max' => 2,
                'message' => '{attribute} должен быть строкой.',
                'tooShort' => '{attribute} должен содержать 2 символа.',
                'tooLong' => '{attribute} должен содержать 2 символа.'],
            [['name'], 'string', 'max' => 255],
            [['code'], 'unique', 'message' => '{attribute} {value} уже существует.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Код страны',
            'name' => 'Название',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::className(), ['country_id' => 'id']);
    }
}
