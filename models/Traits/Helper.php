<?php

namespace app\models\Traits;

trait Helper 
{
    public function calculateCityRequests(array $userRequests): array
    {
        $response = []; 
        foreach($userRequests as $userRequest) {            
            if (array_key_exists($userRequest->city_id, $response)) {
                $response[$userRequest->city_id]['requests'] += 1;    
                continue;
            }

            $response[$userRequest->city_id]['city'] = $userRequest->city->name;
            $response[$userRequest->city_id]['country'] = $userRequest->city->country->code;
            $response[$userRequest->city_id]['requests'] = 1;
        }

        return array_values($response);
    }

    public function formatDate($from, $to)
    {        
        $from = $this->formatter($from);
        $to = $this->formatter($to);        

        return [$from, $to];
    }

    private function formatter($date)
    {       
        if ($this->isValidDate($date)) {
            return $date;
        }

        if ($this->isValidTimeStamp($date)) {
            return date("Y-m-d", (int)$date);
        }

        $date = str_replace(['/', '-'], '.', $date);
        return date("Y-m-d", strtotime($date));
    }

    private function isValidDate($date, $format = 'Y-m-d')
    {
        $d = \DateTime::createFromFormat($format, $date);        
        return $d && $d->format($format) === $date;
    }

    private function isValidTimeStamp($timestamp)
    {
        return ((string) (int) $timestamp === $timestamp) 
            && ($timestamp <= PHP_INT_MAX)
            && ($timestamp >= ~PHP_INT_MAX);
    }
}
