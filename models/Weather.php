<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "weather".
 *
 * @property int $id
 * @property int $city_id
 * @property string|null $for_date
 * @property int $temperature
 *
 * @property City $city
 */
class Weather extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'weather';
    }

    public function fields()
    {
        return [
            'Город' => function () {
                return $this->city->name;
            },
            'Страна' => function () {
                return $this->city->country->code;
            },
            'Дата' => 'for_date',
            'Температура' => 'temperature',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city_id', 'temperature'], 'required'],
            [['city_id', 'temperature'], 'integer'],
            [['for_date'], 'safe'],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id'],
                'message' => '{attribute} не существует.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'Идентификатор города',
            'for_date' => 'Дата',
            'temperature' => 'Температура',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }
}
